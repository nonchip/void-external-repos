#!/bin/sh

external_repos_folder=~/.local/share/void-external-repos
upstream_url="https://github.com/void-linux/void-packages.git"

_init() {
  mkdir -p "${external_repos_folder}"
  touch "${external_repos_folder}/.packages"
  mkdir -p "${external_repos_folder}/.master"
  mkdir -p "${external_repos_folder}/.host"
  _add_repo upstream "$upstream_url" || exit 1
  cd "${external_repos_folder}/upstream" || exit 1
  ./xbps-src -m "${external_repos_folder}/.master" -H "${external_repos_folder}/.host" binary-bootstrap || exit 1
}

_add_repo() { # <name> <url>
  mkdir -p "${external_repos_folder}/$1"
  git clone \
  --depth 1 \
  --filter=blob:none \
  --no-checkout --sparse \
  "$2" "${external_repos_folder}/$1" || exit 1
  cd "${external_repos_folder}/$1" || exit 1
  git sparse-checkout init --cone || exit 1
  git sparse-checkout set common etc xbps-src srcpkgs/xbps-triggers || exit 1
  git checkout || exit 1
}

_fetch_branch() { # <name> <branch>
  cd "${external_repos_folder}/$1" || exit 1
  git fetch -fu origin "$2:$2" || exit 1
}

_add_package() { # <name> <branch> <package>
  cd "${external_repos_folder}/$1" || exit 1
  git sparse-checkout add "srcpkgs/$3" || exit 1
}

_checkout_branch() { # <name> <branch>
  cd "${external_repos_folder}/$1" || exit 1
  git checkout "$2" || exit 1
  git reset --hard || exit 1
}

_xbps_src() { # <name> <branch> <command>
  cd "${external_repos_folder}/$1" || exit 1
  repo="$1"
  branch="$2"
  shift 2 || exit 1
  exec ./xbps-src -m "${external_repos_folder}/.master" -H "${external_repos_folder}/.host" -r "$repo/$branch" "$@"
}

_src_cmd() {
  line="$(grep "^$1 " "${external_repos_folder}/.packages")" || {
    echo "Error: package '$1' unknown." > /dev/stderr
    exit 1
  }
  shift 1
read pack repo branch <<_EOT_
$line
_EOT_
  _fetch_branch "$repo" "$branch" || exit 1
  _checkout_branch "$repo" "$branch" "$pack" || exit 1
  _xbps_src "$repo" "$branch" "$@"
}

_list_branches() { # <reponame>
  cd "${external_repos_folder}/$1" || exit 1
  git ls-remote --heads origin | cut -d/ -f 3
}

test -d "${external_repos_folder}" || _init

case "$1" in
  add-repo) # <name> <url>
    test -d "${external_repos_folder}/$2" && {
      echo "Error: repo '$2' exists." > /dev/stderr
      exit 1
    }
    _add_repo "$2" "$3"
    ;;
  ls-branches) # <reponame>
    test -d "${external_repos_folder}/$2" || {
      echo "Error: repo '$2' does not exist." > /dev/stderr
      exit 1
    }
    _list_branches "$2"
    ;;
  index)
    for repo in ${external_repos_folder}/*
      do cd $repo
      repo=$(basename $repo)
      for branch in $(_list_branches $repo)
        do echo -n "$repo $branch "
        git fetch --depth=1 origin refs/heads/$branch:refs/remotes/origin/$branch
      done
    done
    ;;
  search) # <term>
    for repo in ${external_repos_folder}/*
      do cd $repo
      repo=$(basename $repo)
      for branch in $(_list_branches $repo)
        do if git rev-parse --verify --quiet origin/$branch:srcpkgs/"$2" >/dev/null
          then echo "$repo $branch"
        fi
      done
    done
    ;;
  add-package) # <reponame> <branch> <package>
    test -d "${external_repos_folder}/$2" || {
      echo "Error: repo '$2' does not exist." > /dev/stderr
      exit 1
    }
    grep "^$4 " "${external_repos_folder}/.packages" && {
      echo "Error: package '$4' already found." > /dev/stderr
      exit 1
    }
    _fetch_branch "$2" "$3" || exit 1
    _add_package "$2" "$3" "$4" || exit 1
    echo "$4 $2 $3" >> "${external_repos_folder}/.packages"
    ;;
  pkg) # <package>

    _src_cmd "$2" pkg "$2"
    ;;
  src) # <package> <remainder>
    shift 1
    _src_cmd "$@"
    ;;
  i) # <package>
    line="$(grep "^$2 " "${external_repos_folder}/.packages")" || {
      echo "Error: package '$2' unknown." > /dev/stderr
      exit 1
    }
read pack repo branch <<_EOT_
$line
_EOT_
    xbps-install -iR "${external_repos_folder}/.host/binpkgs/$repo/$branch" "$pack"
    ;;
  *)
    cat <<_EOT_
USAGE:
  $0 <operation> <arguments>
Operations:
  add-repo <name> <url>                    Add the repository pointed to by <url> as <name>
  add-package <repo> <branch> <package>    Add <package> from the repository named <repo>'s branch <branch>
  ls-branches <repo>                       Lists all branches in <repo>
  index                                    Updates the search index (super slow!)
  search <package>                         Searches all branches of all repos for <package>.
  src <package> <...>                      Call \`xbps-src <...>\` in the environment for <package>
  pkg <package>                            shorthand for \`src <package> pkg <package>\`
  i <package>                              Install the built <package>
_EOT_
  ;;
esac
